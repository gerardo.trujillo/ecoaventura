import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { FooterComponent } from "./public/footer/footer.component";
import { NavbarComponent } from "./public/navbar/navbar.component";



@NgModule({
    declarations: [
      NavbarComponent,
      FooterComponent
    ],
    exports: [
      NavbarComponent,
      FooterComponent
    ],
    imports: [
      CommonModule,
      RouterModule
    ]
})
export class ComponentsModule { }
