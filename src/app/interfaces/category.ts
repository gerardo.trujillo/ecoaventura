export interface Category {
  id: number;
  parent_id: number;
  name: {
    fr: string,
    de: string,
  };
  slug: {
    fr: string,
    de: string,
  };
  description: {
    fr: string,
    de: string,
  };
  type: string;
  image: string;
  active: boolean;
  status: boolean;
}
