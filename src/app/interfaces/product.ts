export interface Product {
  id: number;
  name: {
    fr: string;
    de: string;
  };
  slug: {
    fr: string;
    de: string;
  };
  new: boolean;
  novelty: boolean;
  key: string;
  price: number;
  size: string;
  float: number;
  model: string;
  description: {
    fr: string;
    de: string;
  };
  details: {
    fr: string;
    de: string;
  };
  image: string;
  active: boolean;
  category:{
    name: {
      fr: string;
      de: string;
    };
    slug: {
      fr: string;
      de: string;
    };
  }

}
