export interface ParentCategory {
  name: {
    es: string,
    fr: string,
    de: string
  }
}
