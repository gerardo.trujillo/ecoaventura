export interface Event {
  id: number;
  title: string;
  slug: string;
  intro: string;
  description: string;
  date: string;
  datet: object;
  image: string;
  active: boolean;
  time: object;

}
