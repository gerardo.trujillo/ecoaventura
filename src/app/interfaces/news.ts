export interface News {
  id: number
  title: {
    fr: string;
    de: string;
  };
  slug: {
    fr: string;
    de: string;
  };
  intro: {
    fr: string;
    de: string;
  };
  content: {
    fr: string;
    de: string;
  };
  date: string;
  image: string;
  active: boolean;
  type: string;
}
