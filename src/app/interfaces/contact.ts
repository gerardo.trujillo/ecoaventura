export interface Contact {
  name: string;
  telephone: string;
  email: string;
  message: string;
}
