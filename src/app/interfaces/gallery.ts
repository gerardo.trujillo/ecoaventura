export interface Gallery {
  id: number;
  name: {
    fr: string;
    de: string;
  };
  slug: {
    fr: string;
    de: string;
  };
  type: string;
  reference: number;
  active: boolean
}
