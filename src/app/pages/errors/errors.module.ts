import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErrorsRoutingModule } from './errors-routing.module';
import { ErrorsComponent } from './errors.component';
import { E404Component } from './e404/e404.component';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [
    ErrorsComponent,
    E404Component
  ],
    imports: [
        CommonModule,
        ErrorsRoutingModule,
        TranslateModule
    ]
})
export class ErrorsModule { }
