import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicComponent } from './public.component';
import { MainComponent } from "./main/main.component";
import { ContactComponent } from "./contact/contact.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { PoliticsComponent } from "./politics/politics.component";

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        component: MainComponent
      },
      {
        path: 'aviso-privacidad',
        component: PrivacyComponent
      },
      {
        path: 'politicas',
        component: PoliticsComponent
      },
      {
        path: 'contacto',
        component: ContactComponent
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'errors/404'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
