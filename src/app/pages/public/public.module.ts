import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { MainComponent } from './main/main.component';
import { ComponentsModule} from "../../components/components.module";
import { ContactComponent } from './contact/contact.component';
import { TranslateModule} from "@ngx-translate/core";
import { IvyGalleryModule } from "angular-gallery";
import { PrivacyComponent } from "./privacy/privacy.component";
import { PoliticsComponent } from "./politics/politics.component";
import { AboutComponent } from "./about/about.component";
import {FormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {RecaptchaModule} from "ng-recaptcha";


@NgModule({
  declarations: [
    PublicComponent,
    MainComponent,
    PrivacyComponent,
    PoliticsComponent,
    AboutComponent,
    ContactComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    ComponentsModule,
    TranslateModule,
    IvyGalleryModule,
    FormsModule,
    NgbModule,
    RecaptchaModule
  ]
})
export class PublicModule { }
