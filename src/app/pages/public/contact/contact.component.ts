import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Title } from "@angular/platform-browser";
import { Alert } from "../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { Contact } from "../../../interfaces/contact";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  alert:Alert=<Alert>{};
  recaptcha:boolean=false;
  contact:Contact=<Contact>{};

  constructor(private http: HttpClient,
              private title: Title,
              private loading: NgxSpinnerService) {
    this.title.setTitle("Contacto");
  }

  ngOnInit(): void {
  }

  sendEmail(form: NgForm) {
    this.loading.show();
    if (this.recaptcha){
      this.alert = {
        active: true,
        type: 'success',
        message: 'El mensaje se a enviado correctamente'
      };
      return this.http.post('https://mandrillapp.com/api/1.0/messages/send.json',
        {
          key: '3NShetxNNLLYhYNU3A3gqg',
          message: {
            html: '<p>Example HTML content</p>',
            subject: 'example subject',
            from_email: 'message.keplerreyeshern@gmail.com',
            from_name: 'Example Name',
            to: [
              {
                email: 'recipient.keplerreyeshern@gmail.com',
                name: 'Recipient Name',
                type: 'to'
              }
            ],
            headers: {
              'Reply-To': 'message.reply@example.com'
            }
          }

        });
    } else {
      this.alert = {
        active: true,
        type: 'danger',
        message: 'Debes comprobar que no eres un robot'
      };
      return false;
    }
    this.loading.hide();
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.recaptcha = true;
  }

}
