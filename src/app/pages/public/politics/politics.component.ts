import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-politics',
  templateUrl: './politics.component.html',
  styleUrls: ['./politics.component.sass']
})
export class PoliticsComponent implements OnInit {

  constructor(private title: Title) {
    this.title.setTitle("Politicas");
  }

  ngOnInit(): void {
  }

}
