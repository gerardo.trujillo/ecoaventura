import {Component, OnInit, ViewChild} from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import { Product } from "../../../interfaces/product";
import { environment } from "../../../../environments/environment";
import { TranslationService } from "../../../services/admin/translation.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  // images = ['assets/images/ecoturismo.jpg', 'assets/images/nuevas-emociones.jpg', 'assets/images/te-gusta-la-aventura.jpg'];

  products:Product[]=[];
  url_images = environment.baseUrl;

  constructor(private title: Title,
              private meta: Meta,
              public serviceTranslation: TranslationService) {
    this.title.setTitle("Eco Aventuras Méxicanas");
    this.meta.addTags([
      { name: 'keywords', content: 'Turismo, Aventura, México, EcoAventura' },
      { name: 'author', content: 'Excess' },
      { name: 'description', content: 'Turismo en México' },
      { name: 'lang', content: 'es' },
      { property:'og:image', content: 'https://ecoaventuramx.matrix-g.com/assets/images/logo.png' }
    ]);
  }

  ngOnInit(): void {
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/default-user-image.png'
  }

}
