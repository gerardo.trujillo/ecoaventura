import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {PasswordService} from "../../../../services/auth/password.service";
import {NgxSpinnerService} from "ngx-spinner";
import {Alert} from "../../../../interfaces/alert";

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.sass']
})
export class EmailComponent implements OnInit {

  alert: Alert = <Alert>{};

  constructor(private loading: NgxSpinnerService,
              private service: PasswordService) { }

  ngOnInit(): void {
  }

  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('email', form.value.email);
    this.service.createPassword(params).subscribe( response => {
      this.alert.message = response.message;
      this.alert.type = 'success';
      this.alert.active = true;
      form.resetForm();
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.alert.message ='Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.loading.hide();
      } else if(err.status == 404){
        this.alert.message = err.error.message;
        this.alert.active = true;
        this.alert.type = 'danger';
        form.resetForm();
        this.loading.hide();
      } else {
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.loading.hide();
      }
    });
  }

  closeAlert() {
    this.alert.active = false;
  }

}
