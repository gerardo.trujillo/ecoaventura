import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {DataService} from "./services/data/data.service";
import {NgxSpinnerService} from "ngx-spinner";
import {loader} from "ng-recaptcha/lib/load-script";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'EcoAventura';
  activeLang = 'es';

  constructor(private translate: TranslateService,
              private serviceData: DataService,
              private loading: NgxSpinnerService,
              private setTitle: Title,
              private meta: Meta) {
    if (typeof this.activeLang === 'string') {
      this.translate.setDefaultLang(this.activeLang);
      localStorage.setItem('isLoggedIn', 'false');
      localStorage.setItem('language', 'es');
    }
  }

}
