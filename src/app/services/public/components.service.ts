import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  baseUrl = environment.baseUrl + '/api/components';

  constructor(private http: HttpClient) {

  }

  getData(){
    return this.http.get<any>(this.baseUrl);
  }

  setClick(type: string){
    return this.http.get<any>(this.baseUrl + '/clicks/' + type);
  }
}
